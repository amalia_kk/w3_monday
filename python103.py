## Lists
# A list is a numerically organised list of objects. They are organised using indeces that start at zero. It can be anything from a shopping list to a list of crazy ex landlords.

# syntax []
from calendar import c
from hashlib import new
from operator import ne


print(type([]))

# YOu can define a list with several itesms
crazy_landlords = ['Karen', "Bob", "Lucinda", "Simon"]
print(crazy_landlords)


# Organised with indeces, starting from 0:
crazy_landlords = ['Karen', "Bob", "Lucinda", "Simon"]
print("First in the list is", crazy_landlords[0])
print("Third in the list is", crazy_landlords[2])
print("Last in the list is", crazy_landlords[-1])

# Add something to the list
# .append()
crazy_landlords.append('Shenice')
print(len(crazy_landlords))


# Remove something from list 
# .pop()

crazy_landlords.pop()  # Removes last item on list
print(crazy_landlords)


# Remove using index 
crazy_landlords.pop(0)
print(crazy_landlords)


# Lists are mutable and can take multiple data types

new_list = [10, 11, 20, "hello", "multiple data", [10, 20, 'yay']]
print(new_list)
print(type(new_list[3]))


# Lists are mutable and you can reassign index position
newer_list = 543543
print(newer_list)