# Strings

## Strings are a list of unorganised characters that can have spaces, numbers and any characters
## The syntax is simple, "" or ''

from re import M
from tkinter import N


print('this is a string')
print(type("This is also a string"))



my_string = "this is a nice simple string"

# len() -> length of string

print(len(my_string))




# Useful string methods
# object.capitalize()

print(my_string)
print(my_string.capitalize())
print('^ done using the capitalize function')

# .lower() --> all characters lower

print(my_string.lower())


# .upper() --> capitalises all letters

print(my_string.upper())


# .title()

print(my_string.title())



## Strings can be concactenated 
# The joining of two dtrinfs

name = "Amalia"
greeting = "Aloha," 
print(greeting + ' ' + name)



# Interpolation
# You can interpolate into a string value using f strings (formatted strings)
# Syntax print(f""") 
print(f"Hello and welcome <name>!")
print(f"Hello and welcome {name.upper()}!")

# Strings, as said at the start, are lists of characters. But they are lists.
print(name[0])
print(name[4])



# Numerical types
# These are numbers. We have integers, floats and a few others that are less used. You can perform mathematical arithmetic with them.

my_number = 16
print(my_number)
print(type(my_number))

numa = 8
numb = 2
print(numa + numb)
numc = numa - numb
print(numc)
numd = numa / 2
# All of the above are integers



# Floats. They're composite numbers. Every time you divide, you get a float.

print(type(numd))
print(type(3.14))

# Ther are comparison operators that you can use logically with maths such as greater than, smaller than and so on. The result is called a boolean
# A boolean is a data type that is either true or false. 

print(type(True))
print(type(False))

print(numa > numb)

print(numa >= 2)

print(numa == numb)  # Can also use ===

print(numa != numb)  # Filipe doesn't like this notation



# The comparison operator also works for strings

print('Is the string 10 equal to integer 10 in py?', '10' == 10)

example_input = 10
user_inp = input("> provide input: ")
print(example_input == user_inp)
print(type(example_input))
print(typ(user_inp))


# Casting helps us change the data type where possible. Very useful when taking in user inputs
# User input is captured as a string while the exmaple input is captured as a integer

## Change an integer to a string
# str()

example_int = 10
example_str = str(example_int)
print(type(example_str))



# Change a string to an integer
# Will only work with clean strings using numerical types only
print(type(int('10')))



