## print() command
## the print command makes code output to the terminal

print('hello')


## ^ does not mean that code cannot run withouth outputting to the terminal

10 * 100
20 * 33
print (20*2) #only 40 will be displayed when you run the program


## type
# Help you to identify the type of data. For example:
print(10, 'hello')
print(type(10))
print('10')
print(type('10'))



## Variables
# A variable is like a box. It has a name, we can put stuff inside and also get the stuff out of the box. Also, at any point we can put other stuff in the same box.
# We could have a box called Books and put "Rich Dad Poor Dad" inside.

# Assignment of variable Books to a string
books = "Rich Dad Poor Dad"

# Calling of the variable
print(books)
print(type(books))


# Prompting user for an input

a = input('Give me a  number: ')
