# Python 101

Python is a programming language that is syntax light and easy to pickup. It is very versatile and well-paid. Used across the industry for:

- Simply scripts
- Automation (ansibles)
- Web development 
- Data and ML
- Others


List of things to cover this week:

- Data types
- Strings
- NUmericals
- Booleans
- List
- Dictionaries
- If conditions
- Loops
- While loops
- Functions
- TDD & unitesting
- Error handling
- External packages
- APIs


Other topics:

- TDD
- Polymorphism
- Dry code
- Separation of concerns
- ETL- extract transform and load
- Frameworks (difference to actual languages)



## <u>Python basics</u>

You can make files and run them, you can run on the command line or you can have a framework running python. 

To try stuff out super quickly, use the command line.

```bash
python3
>>>
```

Then you can write in python:
```python
>>> human = "Michael"
>>> print(human)
Michael
```

